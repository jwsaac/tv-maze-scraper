# Tv Maze Scraper
This Scraper are consists of the following applications

1. Console Application - The console application is responsible for scraping the tv show information with their casts from TvMaze API and saving it into SQL server.
2. Web API - The API exposes an endpoint that provides tv show information with their casts, the data is coming from the SQL server in which the console application saves
the data.


# How to start.
1. Import database schema into your local SQL Server. The schema is located in the database folder.
2. Edit the connection string located in the environment variables of the Console app and the API.
3. Run first the console application to start scraping data and saving those data into the database.
4. Endpoints in the API can now be used to provide data coming from the database.

#Tools
- Visual studio version: 16.11.5
- C# 9
- .NET 5


