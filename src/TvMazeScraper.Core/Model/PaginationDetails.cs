﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Core.Model
{
    public class PaginationDetails
    {
        public PaginationDetails(int page, int pagSize) 
        {
            Page = page;
            PageSize = pagSize;
        }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
