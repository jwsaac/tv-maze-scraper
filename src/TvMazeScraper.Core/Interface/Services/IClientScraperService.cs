﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Core.Interface.Services
{
    public interface IClientScraperService : IDisposable
    {
        Task StartScraping();
    }
}
