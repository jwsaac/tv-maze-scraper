﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvMazeScraper.Core.Entities;
using TvMazeScraper.Core.Model;

namespace TvMazeScraper.Core.Interface.Repositories
{
    public interface IShowInfoRepository
    {
        Task<List<Show>> Get(PaginationDetails pagination);

        Task<Show> Get(int id);
        void Add(Show s);
        Task SaveEntities();
    }
}
