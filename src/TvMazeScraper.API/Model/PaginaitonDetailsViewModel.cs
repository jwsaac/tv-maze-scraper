﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using TvMazeScraper.Core.Model;

namespace TvMazeScraper.API.Model
{
    public class PaginaitonDetailsViewModel
    {
        public PaginaitonDetailsViewModel() 
        {
            //Default value
            PageSize = 200;
            Page = 1;
        }

        [Range(1, 200, ErrorMessage = "Invalid page size.")]
        public int PageSize { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Invalid page.")]
        public int Page { get; set; }


        public PaginationDetails Map() 
        {
            return new PaginationDetails(Page, PageSize);
        }
    }
}
