﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TvMazeScraper.API.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class GlobalErrorsController : ControllerBase
    {
        ILogger<GlobalErrorsController> _logger;
        public GlobalErrorsController(ILogger<GlobalErrorsController> logger) 
        {
            _logger = logger;
        }
        [HttpGet]
        [Route("/errors")]
        public IActionResult HandleErrors() 
        {
            var contextException = HttpContext.Features.Get<IExceptionHandlerFeature>();

            _logger.LogCritical(contextException.Error, "Undhandled Exception!");

            return Problem("Internal Server Error.");
        }
    }
}
