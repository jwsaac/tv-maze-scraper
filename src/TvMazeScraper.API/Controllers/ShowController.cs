﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Core.Interface.Repositories;
using TvMazeScraper.API.Model;

namespace TvMazeScraper.API.Controllers
{
    [Route("api")]
    [ApiController]
    public class ShowController : ControllerBase
    {
        IShowInfoRepository _repository;
        public ShowController(IShowInfoRepository repository) 
        {
            _repository = repository;
        }


        /// <summary>
        /// Get show information including the cast
        /// </summary>
        /// <returns>s</returns>
        [HttpGet("shows")]
        public async Task<ActionResult> Get([FromQuery] PaginaitonDetailsViewModel paginationVM)
        {
            var shows = await _repository.Get(paginationVM.Map());

            if (shows == null || shows.Count() == 0)
                return NotFound();

            return Ok(shows);
        }


        /// <summary>
        /// Get show information including the cast using show id
        /// </summary>
        /// <returns>s</returns>
        [HttpGet("shows/{id}")]
        public async Task<ActionResult> Get([FromRoute] int id)
        {
            var show = await _repository.Get(id);

            if (show == null)
                return NotFound();

            return Ok(show);
        }
    }
}
