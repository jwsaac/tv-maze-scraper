﻿using System;
using System.Threading.Tasks;
using TvMazeScraper.Core.Interface.Services;
using TvMazeScraper.Core.Interface.Repositories;
using TvMazeScraper.Infrastructure.Repositories;
using TvMazeScraper.Infrastructure.Services.Scraper;
using TvMazeScraper.Infrastructure.Services.Scraper.Settings;
namespace TvMazeScraper.Client
{
    class Program
    {

        static async Task Main(string[] args)
        {
            var scraperSettings = new ScraperSettings() { 
                ApiBaseUrl = @"https://api.tvmaze.com",
                ShowsRoute = @"/shows?page={page}",
                CastRoute = @"/shows/{id}/cast",
                MaxPage = 2
            };
            string connectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");

            IShowInfoRepository repository = new MsSQLShowInfoRepository(connectionString);


            Console.WriteLine("Scraping Tv show information started...");

            using (IClientScraperService scraper = new ClientScraperService(scraperSettings,repository)) 
            {
                try
                {
                    await scraper.StartScraping();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                
            }
            Console.WriteLine("Scraping Tv show information completed!");
            Console.ReadLine();
        }
    }
}

