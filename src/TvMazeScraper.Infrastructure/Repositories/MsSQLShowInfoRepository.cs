﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TvMazeScraper.Core.Entities;
using TvMazeScraper.Core.Model;
using TvMazeScraper.Core.Interface.Repositories;

namespace TvMazeScraper.Infrastructure.Repositories
{
    public class MsSQLShowInfoRepository : IShowInfoRepository
    {
        string _connectionString;
        string _tableName = "ShowInfo";
        public MsSQLShowInfoRepository(string connectionString)
        {
            _connectionString = connectionString;
        }


        List<Show> Shows = new List<Show>();

        public void Add(Show s)
        {
            Shows.Add(s);
        }

        public async Task SaveEntities()
        {
            string commandText = $"INSERT INTO {_tableName} VALUES (@data)";
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(commandText, connection))
                {
                    foreach (var show in this.Shows)
                    {
                        var serializedData = JsonSerializer.Serialize(show);

                        command.Parameters.AddWithValue("@data", serializedData);

                        command.ExecuteNonQuery();

                        command.Parameters.Clear();
                    }
                }
            }
        }

        public async Task<List<Show>> Get(PaginationDetails pagination)
        {
            int offset = (pagination.Page - 1) * pagination.PageSize; 
            string query = $"SELECT * FROM {_tableName} ";
            query += "ORDER BY Id ";
            query += $"OFFSET {offset} ROWS ";
            query += $"FETCH NEXT {pagination.PageSize} ROWS ONLY ";


            var shows = await ExecuteQuey(query);

            return shows;
        }

        public async Task<Show> Get(int id)
        {
            string query = $"SELECT * FROM {_tableName} WHERE Id = {id}";

            var shows = await ExecuteQuey(query);

            return shows.FirstOrDefault();
        }

        public async Task<List<Show>> ExecuteQuey(string query)
        {
            var showInfoList = new List<Show>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(query, connection))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader[0]);
                        string dataString = Convert.ToString(reader[1]);
                        Show deserializedData = JsonSerializer.Deserialize<Show>(dataString);
                        deserializedData.Id = id;

                        showInfoList.Add(deserializedData);
                    }
                }
            }

            return showInfoList;
        }
    }
}
