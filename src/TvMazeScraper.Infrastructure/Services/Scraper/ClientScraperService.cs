﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.Json;
using TvMazeScraper.Core.Entities;
using TvMazeScraper.Core.Interface.Services;
using TvMazeScraper.Core.Interface.Repositories;
using TvMazeScraper.Infrastructure.Services.Scraper.Settings;
using TvMazeScraper.Infrastructure.Services.Scraper.Model;
using Polly.Retry;
using Polly;

namespace TvMazeScraper.Infrastructure.Services.Scraper
{
    public class ClientScraperService : IClientScraperService
    {
        private int _maxRetries = 3;
        private HttpClient _client;
        private ScraperSettings _scraperSettings;
        private JsonSerializerOptions _serializerOptions;
        private IShowInfoRepository _showRepository;
        private readonly AsyncRetryPolicy _retryPolicy;
        
        public ClientScraperService(ScraperSettings settings,IShowInfoRepository showRepository)
        {
            _scraperSettings = settings;
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_scraperSettings.ApiBaseUrl);

            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            _serializerOptions = options;

            _showRepository = showRepository;

            _retryPolicy = Policy.Handle<HttpRequestException>().RetryAsync(retryCount: _maxRetries);
        }
        public async Task StartScraping()
        {
            List<Show> showInfoList = new List<Show>();

           
            for (int page = 1; page <= _scraperSettings.MaxPage; page++) 
            {
                Console.WriteLine($"Fetching shows on page {page}...");
                var showModelList = await GetShowsAsync(page);
                Console.WriteLine($"{showModelList.Count()} shows has been fetched...");

                foreach (var showModel in showModelList)
                {
                   
                    var showInfo = showModel.MapToEntity();

                    Console.WriteLine($"Fetching casts on show {showModel.Name}...");

                    var castModelList = await GetCasts(showModel.Id);

                    Console.WriteLine($"{castModelList.Count()} casts has been fetched...");

                    var casts = castModelList.Select(c => c.MapToEntity()).ToList();

                    showInfo.Casts.AddRange(casts);

                    _showRepository.Add(showInfo);
                }
            }


            Console.WriteLine("Saving show information to database...");
            await _showRepository.SaveEntities();
            Console.WriteLine("Saved successfully!");
        }


        private async Task<ShowModel[]> GetShowsAsync(int page)
        {
            return await _retryPolicy.ExecuteAsync(async () =>
            {
                string requestUrl = _scraperSettings.ShowsRoute.Replace("{page}", page.ToString());
                var response = await _client.GetAsync(requestUrl);

                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return null;

                var jsonString = await response.Content.ReadAsStringAsync();

                var shows = JsonSerializer.Deserialize<ShowModel[]>(jsonString, _serializerOptions);

                return shows;
            });
        }

        private async Task<CastModel[]> GetCasts(int id)
        {
            return await _retryPolicy.ExecuteAsync(async () =>
            {

                var response = await _client.GetAsync(_scraperSettings.CastRoute.Replace("{id}", id.ToString()));

                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return null;

                var jsonString = await response.Content.ReadAsStringAsync();

                var casts = JsonSerializer.Deserialize<CastModel[]>(jsonString, _serializerOptions);

                return casts;
            });

        }

      

        public void Dispose()
        {
            _client.Dispose();
        }
    }
    
}
