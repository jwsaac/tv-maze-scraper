﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvMazeScraper.Core.Entities;

namespace TvMazeScraper.Infrastructure.Services.Scraper.Model
{
    public class ShowModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Show MapToEntity() 
        {

            return new Show { Id = this.Id, Name = this.Name}; 
        }
    }
}
