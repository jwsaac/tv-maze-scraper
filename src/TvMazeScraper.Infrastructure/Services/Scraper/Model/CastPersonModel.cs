﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure.Services.Scraper.Model
{
    public class CastPersonModel
    {
        public string Name { get; set; }
    }
}
