﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TvMazeScraper.Core.Entities;
namespace TvMazeScraper.Infrastructure.Services.Scraper.Model
{
    public class CastModel
    {
        public CastPersonModel Person { get; set; }

        public Cast MapToEntity() 
        {
            return new Cast { Name = this.Person.Name };
        }
    }
}
