﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure.Services.Scraper.Settings
{
    public class RateLimitSettings
    {
        public int CallsPerSecond { get; set; }
    }
}
