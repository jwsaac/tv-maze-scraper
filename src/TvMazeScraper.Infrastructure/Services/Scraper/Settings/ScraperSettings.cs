﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure.Services.Scraper.Settings
{
    public class ScraperSettings
    {
        //public ScraperSettings(string apiBaseUrl, string showsRoute, string castRoute, int maxPage) 
        //{
        //    ApiBaseUrl = apiBaseUrl;
        //    ShowsRoute = showsRoute;
        //    CastRoute = CastRoute;
        //    MaxPage = maxPage;
        //}
        public string ApiBaseUrl { get; set; }
        public string ShowsRoute { get; set; }
        public string CastRoute { get; set; }
        public int MaxPage { get; set; }
    }
}
